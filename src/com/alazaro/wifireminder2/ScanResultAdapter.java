package com.alazaro.wifireminder2;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * This class extends and customizes {@link ArrayAdapter} to show the data
 * obtained from the networks and wrapped with {@link ScanResult}.
 * 
 * @author Alvaro Lazaro
 * 
 */
public class ScanResultAdapter extends ArrayAdapter<ScanResult> {

	private List<ScanResult> objects;
	private int textViewResourceId;

	public ScanResultAdapter(Context context, int resource,
			int textViewResourceId, List<ScanResult> objects) {
		super(context, resource, textViewResourceId, objects);

		init(textViewResourceId, objects);
	}

	public ScanResultAdapter(Context context, int textViewResourceId,
			List<ScanResult> objects) {
		super(context, textViewResourceId, objects);

		init(textViewResourceId, objects);
	}

	private void init(int textViewResourceId, List<ScanResult> objects) {
		this.objects = objects;
		this.textViewResourceId = textViewResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater vi = (LayoutInflater) this.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(textViewResourceId, null);
		}

		ScanResult item = objects.get(position);

		if (item != null) {
			TextView ssid = (TextView) v.findViewById(R.id.ssid);
			TextView bssid = (TextView) v.findViewById(R.id.bssid);

			if (ssid != null) {
				ssid.setText(item.SSID);
				v.setEnabled(true);
				v.setBackgroundColor(android.R.color.secondary_text_dark_nodisable);
				String capabilities = item.capabilities;
                if(!capabilities.contains("WEP") && !capabilities.contains("WPA")) {
                    v.setTag("NOKEY");
                    v.setBackgroundColor(Utils.BLUE);
                } else if (item.SSID.matches("WLAN_....|JAZZTEL_....")) {
					v.setBackgroundColor(Utils.GREEN);
				} else {
					v.setEnabled(false);
				}
			}
			if (bssid != null) {
				bssid.setText(item.BSSID);
			}
		}

		return v;
	}
}
