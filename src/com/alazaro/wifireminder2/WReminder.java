package com.alazaro.wifireminder2;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.util.List;

/**
 * Main class of the application. Wifi Reminder recovers passwords from
 * WLAN_XXXX and JAZZTEL_XXXX networks.
 * 
 * @author Alvaro Lazaro
 * 
 * @since Android API 4
 */
public class WReminder extends ListActivity {

	/** The id of the dialog which shows the password */
	private static final int DIALOG_PASS = 1;

	protected BroadcastReceiver receiver;
	protected WifiManager wm;
	protected Dialog dialogPass;

    private AdView adView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init(this);
		setContentView(R.layout.main);

        adView = new AdView(this, AdSize.BANNER, getResources().getString(R.string.publisher_id));

        LinearLayout layout = (LinearLayout)findViewById(R.id.layout);

        layout.addView(adView, 0);

        AdRequest adRequest = new AdRequest();
        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
        adView.loadAd(adRequest);
	}

	/**
	 * Start point.
	 * 
	 * @param context
	 *            the current app
	 */
	private void init(Context context) {

		if (dialogPass == null) {
			dialogPass = new Dialog(this);
		}


		// getting the wireless manager
		wm = (WifiManager) this.getSystemService(WIFI_SERVICE);

		// TODO: ask the user for permission.
		if (!wm.isWifiEnabled()) {
			wm.setWifiEnabled(true);
		}

		wm.startScan();

		// a receiver is needed to listen to the results of the scan
		if (receiver == null) {
			receiver = new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {
					ListActivity la = (ListActivity) context;
					List<ScanResult> networks = wm.getScanResults();

					ScanResultAdapter adapter = new ScanResultAdapter(context,
							R.layout.row, networks);

					la.setListAdapter(adapter);
				}

			};
		}

		registerReceiver(receiver, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);

		// creating the menu
		MenuItem updateItem = menu.findItem(R.id.updateItem);
		updateItem.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				if (wm != null) {
					return wm.startScan();
				}
				return false;
			}
		});
		return true;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_PASS:
			return dialogPass;
		default:
			return null;
		}

	}

	@Override
	protected void onListItemClick(ListView l, View v, final int position,
			long id) {
		final List<ScanResult> scanResults = wm.getScanResults();
		if (position != ListView.INVALID_POSITION && v.isEnabled()
				&& scanResults != null && scanResults.size() > position) {

			// obtaining pass for selected network
			final String pass = Utils.calculatePass(scanResults.get(
					position));
            // setting the dialog up
            dialogPass.setTitle(R.string.password);
            dialogPass.setContentView(R.layout.pass_dialog);

            dialogPass.findViewById(R.id.buttonCancel).setOnClickListener(
                    new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            dialogPass.dismiss();
                        }
                    });

            if (!(v.getTag() != null && v.getTag().equals("NOKEY"))) {
                TextView passwordText = (TextView) dialogPass
                        .findViewById(R.id.passText);
                passwordText.setText(pass);
                dialogPass.findViewById(R.id.buttonConnect).setOnClickListener(
                        new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                ScanResult network = scanResults.get(
                                        position);
                                WifiConfiguration config = new WifiConfiguration();
                                config.BSSID = network.BSSID;
                                config.SSID = "\"" + network.SSID + "\"";
                                config.preSharedKey = "\"" + pass + "\"";
                                int id = wm.addNetwork(config);
                                wm.enableNetwork(id, true);
                                wm.saveConfiguration();
                                dialogPass.dismiss();
                            }
                        });
            } else {
                TextView passwordText = (TextView) dialogPass
                        .findViewById(R.id.passText);
                passwordText.setText(R.string.nopass);
                dialogPass.findViewById(R.id.buttonConnect).setOnClickListener(
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ScanResult network = scanResults.get(
                                        position);
                                WifiConfiguration config = new WifiConfiguration();
                                config.BSSID = network.BSSID;
                                config.SSID = "\"" + network.SSID + "\"";
                                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                                int id = wm.addNetwork(config);
                                wm.enableNetwork(id, true);
                                wm.saveConfiguration();
                                dialogPass.dismiss();
                            }
                        }
                );
            }
			this.showDialog(DIALOG_PASS);
		}
	}

	@Override
	protected void onDestroy() {

		unregisterReceiver(receiver);

		super.onDestroy();
	}
}