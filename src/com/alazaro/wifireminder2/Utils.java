package com.alazaro.wifireminder2;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.graphics.Color;
import android.net.wifi.ScanResult;

/**
 * Set of tools and functions used in the app.
 * 
 * @author Alvaro Lazaro
 *
 */
public class Utils {
	
	public static int GREEN = Color.rgb(0, 85, 0);
	public static int BLUE = Color.rgb(0, 0, 90);

	/**
	 * Obtains a String from a hash.
	 * 
	 * @param hash
	 * @return the String obtained from the hash
	 * 
	 * @see {@link MessageDigest}
	 *
	 */
	private static String convertToHex(byte[] hash) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			int halfbyte = (hash[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = hash[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	/**
	 * Algorithm to extract the default password from WLAN_XXXX and JAZZTEL_XXXX
	 * networks. This algorithm was discovered by people at http://www.seguridadwireless.net
	 * 
	 * @param bssid BSSID of the target network
	 * @param ssid SSID of the target NETWORK
	 * @return the password
	 */
	public static String calculatePass(String bssid, String ssid) {
		String bssid2 = bssid.replace(":", "");
		String pass = bssid2;
		pass = "bcgbghgg"
				+ (pass.substring(0, pass.length() - 4)
						+ ssid.substring(ssid.length() - 4) + bssid2)
						.toUpperCase();
		System.out.println("pass: " + pass);
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] md5 = new byte[32];
			try {
				md5 = md.digest(pass.getBytes("iso-8859-1"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			pass = convertToHex(md5).substring(0, 20);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return pass;
	}

	public static String calculatePass(ScanResult scanResult) {
		String bssid = scanResult.BSSID;
		String ssid = scanResult.SSID;

		return calculatePass(bssid, ssid);
	}

}
